const url = 'https://swapi.co/api/';

const {get} = require('../lib/request');
const {getArrays} = require('../lib/getarrays');

function getfilms(){
    get(url, 'films',{},null)
    .then((response) => {
        response.data.results.forEach(async (element, index) => {
           try {
                var planets = await getArrays(element.planets, 0);
                var characters = [];//await getArrays(element.characters, 1);
                var starships = await getArrays(element.starships, 2);
                
                newSchema = {
                name: element.title,
                planets: planets,
                characters: characters,
                starships: starships
                }
                console.log(newSchema);
           } catch (error) {
               console.log(error);
               return;
           }
        });
    })
    .catch((error)=> {
        console.log(error);
    });
}




module.exports = {
    getfilms
}