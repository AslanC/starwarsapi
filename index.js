const http = require('http');
const conection = require('./services/films');
const config = require('./config/index');

const requestListener = function (req, res) {
  res.writeHead(200);
  res.end('Hello, World!');
}

const server = http.createServer(requestListener);
console.log('se ha conectado satisfactoriamente al servido en el puerto: ' + config.port)
conection.getfilms();

server.listen(config.port);
