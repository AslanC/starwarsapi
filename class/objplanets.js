const {get} = require('../lib/request');

async function objPlanet(item){
    try {
        let res = await get(item, '', {}, null);
        var obj = {
        name: res.data.name,
        terrain: res.data.terrain,
        gravity: res.data.diameter,
        population: res.data.population
        }
        return obj
    } catch (error) {
        throw error;
    }
}

module.exports = {
    objPlanet
}